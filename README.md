# monitor-bot
This project is a prototype for a departure monitor for the Wiener Linien. It supports two use cases.
- The user enters an address using the keyboard and the monitor searches for stations and the respective departures
- The user shares his location and the bot finds the 5 nearest stops and lets the user choose it and show the departures as well.

#### APIs
Telegram API:               https://core.telegram.org/
Java Telegram API:          https://github.com/rubenlagus/TelegramBots
OpenMaps API:               https://nominatim.openstreetmap.org/
Wiener Linien Routing API:  https://www.data.gv.at/katalog/dataset/9c203fec-dc0d-412c-a7a3-7fd77d0346f1
Wiener Linien Monitor API:  https://www.data.gv.at/katalog/dataset/add66f20-d033-4eee-b9a0-47019828e698

#### Tools / Frameworks
- Java 14
- Gradle
- Junit 5
- Lombok

#### Local Build
Add an "app.config" file to your resources where you put two variables:

```
 telegram.token={your_token}
 telegram.username={your_username}
```

#### Deployment on any Linux machine

One possibility to deploy it is by adding the bot as a system service. Copy the script *monitorbot* from the deployment folder to */etc/init.d* and adapt the 
path to point to the jar, which has to be copied to the linux machine. Executing *sudo systemctl enable monitorbot* registers the service, starts it automatically
and writes the according logs.