package at.technikum.wien.syi.api.routing;

import at.technikum.wien.syi.api.ApiException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RouteWrapperTest {

    @Test
    public void testRoutingApi() throws ApiException {
        RoutingWrapper api = new RoutingWrapper();
        List<Stop> stops = api.getStopsByAddress("Zieglergasse");

        assertNotNull(stops);
        assertEquals(4, stops.size());
    }

    @Test
    public void testRoutingApiFilter() throws ApiException {
        RoutingWrapper api = new RoutingWrapper();
        List<Stop> stops = api.getStopsByAddress("messe");

        assertNotNull(stops);
        assertEquals(1, stops.size());
        assertEquals("Wien, Messe-Prater", stops.get(0).getName());
    }
}
