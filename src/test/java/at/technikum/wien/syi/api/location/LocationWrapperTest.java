package at.technikum.wien.syi.api.location;

import at.technikum.wien.syi.api.routing.Stop;
import org.junit.jupiter.api.Test;

import java.nio.charset.Charset;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LocationWrapperTest {

    @Test
    public void testMonitorApi(){
        LocationAPI api = new LocationWrapper();
        Address address = api.getAddressForCoordinates(Float.valueOf((float)48.225899),Float.valueOf((float)16.3290622));

        assertNotNull(address);
        assertNotNull(address.getRoad());
        assertEquals("Antonigasse", address.getRoad());
        System.out.println(address.toString());
    }

    @Test
    public void testDifferentCoords(){
        LocationAPI api = new LocationWrapper();
        Address address = api.getAddressForCoordinates(Float.valueOf((float)48.22973),Float.valueOf((float)16.309347));

        assertNotNull(address);
        assertNotNull(address.getRoad());
        assertEquals("Rebenweg", address.getRoad());
        System.out.println(address.toString());
    }

    @Test
    public void testCSVParsing(){
        DistanceCalculator calculator = new DistanceCalculator();
        List<Stop> stopList = calculator.readCsv();
        assertNotNull(stopList);
        assertEquals(1852, stopList.size());
    }

    @Test
    public void testDistance(){
        DistanceCalculator calculator = new DistanceCalculator();
        List<Stop> stops = calculator.findNearbyStops(Float.valueOf((float)48.225899),Float.valueOf((float)16.3290622));

        assertNotNull(stops);
        assertEquals(5, stops.size());
        assertEquals("Antonigasse", stops.get(0).getName());
        assertEquals("Sommarugagasse", stops.get(1).getName());
    }

    @Test
    public void testOutsideVienna(){
        LocationAPI api = new LocationWrapper();
        Float lat = Float.valueOf((float) 47.744306);
        Float lon = Float.valueOf((float) 16.842404);
        Address address = api.getAddressForCoordinates(lat, lon);
        List<Stop> stops = api.getStopsForCoordinates(lat, lon);
        assertNotNull(address);
        assertNotNull(stops);
        assertEquals(5, stops.size());
        assertEquals("Wr. Neustadt Civitas Nova", stops.get(0).getName());
    }

    @Test
    public void testDistanceUtf8(){
        DistanceCalculator calculator = new DistanceCalculator();
        List<Stop> stops = calculator.findNearbyStops(Float.valueOf((float)48.225044),Float.valueOf((float)16.345823));

        assertNotNull(stops);
        assertEquals(5, stops.size());
        assertEquals("Kutschkergasse", stops.get(0).getName());
        assertEquals("Währinger Straße-Volksoper", stops.get(1).getName());
    }

    @Test
    public void testDistance2(){
        DistanceCalculator calculator = new DistanceCalculator();
        List<Stop> stops = calculator.findNearbyStops(Float.valueOf((float)48.22973),Float.valueOf((float)16.309347));
        assertNotNull(stops);
        assertEquals(5, stops.size());
    }
}
