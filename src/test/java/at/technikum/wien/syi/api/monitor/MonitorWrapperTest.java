package at.technikum.wien.syi.api.monitor;

import at.technikum.wien.syi.api.ApiException;
import at.technikum.wien.syi.api.routing.Stop;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MonitorWrapperTest {

    @Test
    public void testMonitorApi() throws ApiException {
        MonitorAPI api = new MonitorWrapper();
        DepartureReturn departureReturn = api.getDepartures(new Stop("60201510","WähringerStraße"));

        assertNotNull(departureReturn);
        List<Line> stops = departureReturn.getDepartures();
        assertNotNull(stops);
        System.out.println(departureReturn);
    }

    @Test
    public void testErrorResponse() {
        MonitorAPI api = new MonitorWrapper();
        Exception exception = assertThrows(ApiException.class, () -> {
            api.getDepartures(new Stop("60201712","Sofie-Lazarsfeld-Straße"));
        });
    }
}
