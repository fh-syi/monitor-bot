package at.technikum.wien.syi.api.routing;

import at.technikum.wien.syi.api.ApiException;

import java.util.List;

public interface RoutingAPI {
    List<Stop> getStopsByAddress(String address) throws ApiException;
}
