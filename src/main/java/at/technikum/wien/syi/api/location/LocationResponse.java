package at.technikum.wien.syi.api.location;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
class LocationResponse {
    @JsonProperty("place_id")
    private Double placeId;
    private String licence;
    @JsonProperty("osm_type")
    private String osmType;
    @JsonProperty("osm_id")
    private Double osmId;
    private String lat;
    private String lon;
    @JsonProperty("display_name")
    private String displayName;
    private Address address;
    @JsonProperty("bounding_box")
    private List<String> boundingbox = null;

}