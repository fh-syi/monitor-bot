package at.technikum.wien.syi.api.monitor;

import lombok.Data;

@Data
public class Attributes {
    private Integer rbl;
}
