package at.technikum.wien.syi.api.monitor;

import lombok.Data;

@Data
public class Departure {
    private DepartureTime departureTime;
    private Vehicle vehicle;
}
