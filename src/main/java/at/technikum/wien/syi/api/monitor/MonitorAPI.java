package at.technikum.wien.syi.api.monitor;

import at.technikum.wien.syi.api.ApiException;
import at.technikum.wien.syi.api.routing.Stop;

import java.util.List;

public interface MonitorAPI {
    DepartureReturn getDepartures(Stop stop) throws ApiException;
}
