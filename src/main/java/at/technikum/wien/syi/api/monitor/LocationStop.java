
package at.technikum.wien.syi.api.monitor;

import lombok.Data;

@Data
public class LocationStop {
    private String type;
    private Geometry geometry;
    private Properties properties;
}
