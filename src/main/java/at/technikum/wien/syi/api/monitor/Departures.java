package at.technikum.wien.syi.api.monitor;

import lombok.Data;

import java.util.List;

@Data
public class Departures {
    private List<Departure> departure;
}
