package at.technikum.wien.syi.api.monitor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MonitorResponse {
    private Data data;
    private Message message;
}
