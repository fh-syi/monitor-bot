package at.technikum.wien.syi.api.location;

import at.technikum.wien.syi.api.routing.Stop;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class LocationWrapper implements LocationAPI{
    static final Logger logger = LoggerFactory.getLogger(LocationWrapper.class);

    private final long timeout = 2000L;
    private ObjectMapper objectMapper;
    private OkHttpClient client;
    private DistanceCalculator distanceCalculator;

    public LocationWrapper() {
        this.objectMapper = new ObjectMapper();
        this.client = new OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.MILLISECONDS)
                .writeTimeout(timeout, TimeUnit.MILLISECONDS)
                .build();
        this.distanceCalculator = new DistanceCalculator();
    }

    @Override
    public Address getAddressForCoordinates(Float lat, Float lon) {
        HttpUrl httpBuilder = HttpUrl.parse("https://nominatim.openstreetmap.org/reverse").newBuilder()
                .addQueryParameter("lat", String.valueOf(lat))
                .addQueryParameter("lon", String.valueOf(lon))
                .addQueryParameter("format", "json")
                .build();

        Call call = client.newCall(new Request.Builder()
                .url(httpBuilder)
                .get()
                .build());

        Address address = null;
        try (Response response = call.execute()) {
            LocationResponse monitorResponse = this.objectMapper.readValue(response.body().string(), LocationResponse.class);
            address = monitorResponse.getAddress();
        } catch (IOException e) {
            logger.error("Exception occurred for location({},{}): {}", lat, lon, e.getMessage());
        }
        return address;
    }

    @Override
    public List<Stop> getStopsForCoordinates(Float lat, Float lon) {
        return this.distanceCalculator.findNearbyStops(lat, lon);
    }


    @Override
    public Optional<Stop> findStopById(String id){
        return this.distanceCalculator.findStopById(id);
    }

}
