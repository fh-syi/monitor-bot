package at.technikum.wien.syi.api.monitor;

import lombok.Data;

@Data
public class Properties {
    private String name;
    private String title;
    private String municipality;
    private Integer municipalityId;
    private String type;
    private String coordName;
    private String gate;
    private Attributes attributes;
}
