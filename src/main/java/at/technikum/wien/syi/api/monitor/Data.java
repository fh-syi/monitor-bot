
package at.technikum.wien.syi.api.monitor;

import java.util.List;
@lombok.Data
public class Data {
    private List<Monitor> monitors;
}
