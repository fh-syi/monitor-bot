package at.technikum.wien.syi.api.monitor;

import lombok.Data;

@Data
public class DepartureTime {
    private String timePlanned;
    private String timeReal;
    private Integer countdown;
}
