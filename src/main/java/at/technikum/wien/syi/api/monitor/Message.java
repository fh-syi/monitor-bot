package at.technikum.wien.syi.api.monitor;

import lombok.Data;

@Data
public class Message {
    private String value;
    private Integer messageCode;
    private String serverTime;
}
