package at.technikum.wien.syi.api.location;

import at.technikum.wien.syi.api.routing.Stop;

import java.util.List;
import java.util.Optional;

public interface LocationAPI {
    Address getAddressForCoordinates(Float lat, Float lon);
    List<Stop> getStopsForCoordinates(Float lat, Float lon);

    Optional<Stop> findStopById(String id);
}
