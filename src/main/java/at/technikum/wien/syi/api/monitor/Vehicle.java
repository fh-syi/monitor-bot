package at.technikum.wien.syi.api.monitor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vehicle {
    private String name;
    private String towards;
    private String direction;
    private String richtungsId;
    private Boolean barrierFree;
    private Boolean realtimeSupported;
    private Boolean trafficjam;
    private String type;
    private Integer linienId;
}
