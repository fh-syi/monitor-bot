package at.technikum.wien.syi.api.routing;

import at.technikum.wien.syi.api.ApiException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static at.technikum.wien.syi.bot.utils.Utils.nullableList;

public class RoutingWrapper implements RoutingAPI {
    static final Logger logger = LoggerFactory.getLogger(RoutingWrapper.class);
    private final long timeout = 2000L;
    private ObjectMapper objectMapper;
    private OkHttpClient client;

    public RoutingWrapper() {
        this.objectMapper = new ObjectMapper();
        this.client = new OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.MILLISECONDS)
                .writeTimeout(timeout, TimeUnit.MILLISECONDS)
                .build();
    }

    @Override
    public List<Stop> getStopsByAddress(String address) throws ApiException {
        HttpUrl httpBuilder = HttpUrl.parse("http://www.wienerlinien.at/ogd_routing/XML_STOPFINDER_REQUEST").newBuilder()
                .addQueryParameter("locationServerActive", "1")
                .addQueryParameter("outputFormat", "JSON")
                .addQueryParameter("type_sf", "any")
                .addQueryParameter("place_sf", "Wien")
                .addQueryParameter("name_sf", address)
                .build();

        List<Point> pointLIst;
        Call call = client.newCall(new Request.Builder()
                .url(httpBuilder)
                .get()
                .build());

        List<Stop> stops;
        Response response = null;
        try{
            response = call.execute();
            stops = getStopsFromResponse(response);
        } catch (IOException e) {
            logger.error("Exception occurred for address: {}: {}", address, e.getMessage());
            throw new ApiException(e.getMessage());
        }finally {
            if( response != null)
                response.close();
        }

        return stops;
    }

    private List<Stop> getStopsFromResponse(Response response) throws IOException, ApiException {
        List<Point> pointLIst = null;
        String responseBody = response.body().string();
        try{
            StopFinderResponse sfResponse = this.objectMapper.readValue(responseBody, StopFinderResponse.class);
            pointLIst = sfResponse.getStopFinder().getPoints();
        }catch (IOException e){
            try{
                StopFinderAlternativeResponse sfResponse = this.objectMapper.readValue(responseBody, StopFinderAlternativeResponse.class);
                Points points = sfResponse.getStopFinder().getPoints();
                if(points != null)
                    pointLIst = Lists.newArrayList(points.getPoint());
            }catch (IOException ex){
                logger.error("Exception occurred for address: {}: {}", "", e.getMessage());
                throw new ApiException(e.getMessage());
            }
        }
        return nullableList(pointLIst).stream()
                .filter(s -> !StringUtils.contains(s.getName(), "(AST)"))
                .filter(s -> StringUtils.equals("stop",s.getAnyType()))
                .map(s -> new Stop(s.getRef().getId(), s.getName()))
                .collect(Collectors.toList());
    }
}
