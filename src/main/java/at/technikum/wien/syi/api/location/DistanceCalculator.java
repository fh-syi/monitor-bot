package at.technikum.wien.syi.api.location;

import at.technikum.wien.syi.api.routing.Stop;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;

class DistanceCalculator {
  final static Logger logger = LoggerFactory.getLogger(DistanceCalculator.class);
  private List<Stop> stopList;

  public DistanceCalculator() {
    this.stopList = readCsv();
  }

  public Optional<Stop> findStopById(String id){
    return stopList.stream()
            .filter(stop -> StringUtils.equals(stop.getId(), id))
            .findFirst();
  }

  public List<Stop> findNearbyStops(Float lat, Float lon) {
    List<Stop> nearbyStops = Lists.newArrayList();
    Map<Double, Stop> distanceMap = Maps.newHashMap();
    //calculate distance to all stops and put them in a map
    stopList.forEach(stop -> distanceMap.put(distFrom(lat, lon, stop.getLat(), stop.getLon()), stop));

    //sort map to return the 5 stops closest to the current location
    SortedSet<Double> keys = new TreeSet<>(distanceMap.keySet());
    for (Double key : keys) {
      if(nearbyStops.size() == 5) break;

      Stop value = distanceMap.get(key);
      nearbyStops.add(value);
      logger.info("Distance {} to {}" ,key, value.getName());
    }
    return nearbyStops;
  }

  //Calculate distance between two coordinates - Taken from https://stackoverflow.com/a/123305
  public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
    double earthRadius = 6371.0; // km (or 3958.75 miles)
    double dLat = Math.toRadians(lat2 - lat1);
    double dLng = Math.toRadians(lng2 - lng1);
    double sindLat = Math.sin(dLat / 2);
    double sindLng = Math.sin(dLng / 2);
    double a =
        Math.pow(sindLat, 2)
            + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2));
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double dist = earthRadius * c;

    return dist;
  }

  List<Stop> readCsv() {
    List<Stop> records = new ArrayList<>();
    try (Reader reader = new InputStreamReader(getClass().getResourceAsStream("/wienerlinien-ogd-haltestellen.csv"), "UTF-8")) {
      CSVParser parser = new CSVParserBuilder()
              .withSeparator(';')
              .withIgnoreQuotations(true)
              .build();

      CSVReader csvReader = new CSVReaderBuilder(reader)
              .withSkipLines(1)
              .withCSVParser(parser)
              .build();

      String[] values = null;
      while ((values = csvReader.readNext()) != null) {
        List<String> valList = Arrays.asList(values);
        records.add(new Stop(valList.get(2), valList.get(3),Double.valueOf(valList.get(6)),Double.valueOf(valList.get(7))));
      }
    } catch (IOException e) {
      logger.error("Error on parsing the csv containing the stops!");
    }
    return records;
  }
}
