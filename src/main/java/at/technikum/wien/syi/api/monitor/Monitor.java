package at.technikum.wien.syi.api.monitor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Monitor {
    private LocationStop locationStop;
    private List<Line> lines;
}
