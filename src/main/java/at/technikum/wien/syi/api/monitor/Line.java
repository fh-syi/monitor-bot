package at.technikum.wien.syi.api.monitor;

import lombok.Data;

@Data
public class Line {
    private String name;
    private String towards;
    private String direction;
    private String platform;
    private String richtungsId;
    private Boolean barrierFree;
    private Boolean realtimeSupported;
    private Boolean trafficjam;
    private Departures departures;
    private String type;
    private Integer lineId;
}
