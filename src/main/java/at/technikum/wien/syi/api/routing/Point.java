package at.technikum.wien.syi.api.routing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Point {

    public String usage;
    public String type;
    public String name;
    public String stateless;
    public String anyType;
    public String sort;
    public String quality;
    public String best;
    public String object;
    public String postcode;
    public String street;
    public Ref ref;
    public String buildingNumber;
    public String mainLoc;
    public String modes;
}
