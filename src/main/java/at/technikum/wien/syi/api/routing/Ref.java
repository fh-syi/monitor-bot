
package at.technikum.wien.syi.api.routing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ref {
    public String id;
    public String gid;
    public String omc;
    public String placeID;
    public String place;
    public String coords;
}
