package at.technikum.wien.syi.api.routing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Stop {
    private String id;
    private String name;
    private Double lat;
    private Double lon;

    public Stop(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
