
package at.technikum.wien.syi.api.routing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StopFinderAlternativeResponse {
    public List<Parameter> parameters;
    public StopFinderAlternative stopFinder;
}
