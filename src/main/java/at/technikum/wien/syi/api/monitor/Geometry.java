package at.technikum.wien.syi.api.monitor;

import lombok.Data;

import java.util.List;

@Data
public class Geometry {
    private String type;
    private List<Float> coordinates = null;
}
