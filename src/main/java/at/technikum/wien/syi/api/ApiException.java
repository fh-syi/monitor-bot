package at.technikum.wien.syi.api;

public class ApiException extends Exception{
    public ApiException(String message) {
        super(message);
    }
}
