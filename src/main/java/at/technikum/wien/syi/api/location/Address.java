package at.technikum.wien.syi.api.location;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {
  @JsonProperty("house_number")
  public String houseNumber;
  public String road;
  public String suburb;
  @JsonProperty("city_district")
  public String cityDistrict;
  public String city;
  public String postcode;
  public String country;
  @JsonProperty("country_code")
  public String countryCode;

  public String getFullStreet(){
    return this.getRoad() + " " + this.getHouseNumber();
  }
}
