package at.technikum.wien.syi.api.monitor;

import at.technikum.wien.syi.api.ApiException;
import at.technikum.wien.syi.api.routing.Stop;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static at.technikum.wien.syi.bot.utils.Utils.nullableList;

public class MonitorWrapper implements MonitorAPI {
    static final Logger logger = LoggerFactory.getLogger(MonitorWrapper.class);
    private final long timeout = 2000L;
    private ObjectMapper objectMapper;
    private OkHttpClient client;

    public MonitorWrapper() {
        this.objectMapper = new ObjectMapper();
        this.client = new OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.MILLISECONDS)
                .writeTimeout(timeout, TimeUnit.MILLISECONDS)
                .build();
    }

    @Override
    public DepartureReturn getDepartures(Stop stop) throws ApiException {
        HttpUrl httpBuilder = HttpUrl.parse("https://www.wienerlinien.at/ogd_realtime/monitor").newBuilder()
                .addQueryParameter("diva", stop.getId())
                .build();

        Call call = client.newCall(new Request.Builder()
                .url(httpBuilder)
                .get()
                .build());

        List<Monitor> monitorList = Lists.newArrayList();
        try (Response response = call.execute()) {
            MonitorResponse monitorResponse = this.objectMapper.readValue(response.body().string(), MonitorResponse.class);
            monitorList = monitorResponse.getData().getMonitors();
        } catch (IOException e) {
            logger.error("Exception occurred for stop ({},{}): {}", stop.getId(), stop.getName(), e.getMessage());
            throw new ApiException(e.getMessage());
        }

        Map<String, List<Line>> map = nullableList(monitorList).stream()
                .flatMap(m -> m.getLines().stream())
                .collect(Collectors.groupingBy(Line::getTowards));


        return new DepartureReturn(nullableList(monitorList).stream()
                .flatMap(m -> m.getLines().stream())
                .sorted(Comparator.comparing(Line::getTowards))
                .collect(Collectors.toList()), map);
    }
}
