package at.technikum.wien.syi.api.monitor;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static at.technikum.wien.syi.bot.utils.Utils.nullableList;

@Data
@AllArgsConstructor
public class DepartureReturn {
    private List<Line> departures;
    private Map<String, List<Line>> departureMap;

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        nullableList(departures).forEach(s -> sb.append(buildLineString(s) + "\n"));
        return sb.toString();
    }

    public String toFormattedString(){
        StringBuilder sb = new StringBuilder();
        if(departures != null && !departureMap.isEmpty()){
            for (Map.Entry<String, List<Line>> e : departureMap.entrySet()){
                List<Line> entries = e.getValue();
                entries.sort(Comparator.comparing(Line::getType));
                entries.forEach(s -> sb.append(buildLineString(s) + "\n"));
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    private String buildLineString(Line line) {
        System.out.println(line.getType());
        StringBuilder sb = new StringBuilder(getEmoji(line.getType()));
        sb.append(line.getName() + " ");
        sb.append(" -> ");
        sb.append(line.getTowards());
        sb.append(" in ");
        final Departures departures = line.getDepartures();
        if(departures != null) {
            int departureCount = departures.getDeparture().size();
            int max = departureCount > 4 ? 4 : departureCount -1;
            for (int i = 0; i < max; i++) {
                Departure departure = departures.getDeparture().get(i);
                sb.append(departure.getDepartureTime().getCountdown() + " ");
            }
        }
        sb.append(" min!");
        return sb.toString();
    }

    private String getEmoji(String type) {
        String emoji = "";
        switch (type){
            case "ptTram":
                emoji = "\uD83D\uDE8A";
                break;
            case "ptBusCity":
                emoji = "\uD83D\uDE8C";
                break;
            case "ptMetro":
                emoji = "\uD83D\uDE87";
                break;
            default:
        }
        return emoji;
    }
}
