package at.technikum.wien.syi.bot.handler;

import com.google.common.collect.Lists;
import org.apache.commons.codec.binary.StringUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

public class HelpHandler extends Handler {
    private static final String HELP_COMMAND = "/help";
    private static final String START_COMMAND = "/start";

    public HelpHandler(TelegramLongPollingBot monitorBot) {
        super(monitorBot);
    }

    @Override
    public boolean isSupported(Update update) {
        Message msg = update.getMessage();
        return msg != null && (StringUtils.equals(msg.getText(), HELP_COMMAND)
                || StringUtils.equals(msg.getText(), START_COMMAND));
    }

    @Override
    public void handle(Update update) {
        Long chatId = update.getMessage().getChatId();
        reply(chatId, "Folgende Aktionen sind unterstuetzt: ");
        Lists.newArrayList(
                new HelpHandler(this.getBot()),
                new TextHandler(this.getBot()),
                new LocationHandler(this.getBot()))
                .forEach(handler -> reply(chatId, handler.getHelp()));
    }

    @Override
    public String getHelp() {
        return "/help - Anzeige aller Aktionen";
    }
}
