package at.technikum.wien.syi.bot.handler;

import at.technikum.wien.syi.api.ApiException;
import at.technikum.wien.syi.api.location.LocationAPI;
import at.technikum.wien.syi.api.location.LocationWrapper;
import at.technikum.wien.syi.api.monitor.DepartureReturn;
import at.technikum.wien.syi.api.monitor.MonitorAPI;
import at.technikum.wien.syi.api.monitor.MonitorWrapper;
import at.technikum.wien.syi.api.routing.RoutingAPI;
import at.technikum.wien.syi.api.routing.RoutingWrapper;
import at.technikum.wien.syi.api.routing.Stop;
import at.technikum.wien.syi.bot.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;
import java.util.Optional;

public class TextHandler extends Handler {
    static final Logger logger = LoggerFactory.getLogger(TextHandler.class);

    private final MonitorAPI monitorAPI;
    private final RoutingAPI routingAPI;
    private final LocationAPI locationAPI;

    public TextHandler(TelegramLongPollingBot monitorBot) {
        super(monitorBot);
        this.locationAPI = new LocationWrapper();
        this.routingAPI = new RoutingWrapper();
        this.monitorAPI = new MonitorWrapper();
    }

    @Override
    public boolean isSupported(Update update) {
        boolean supported = false;

        if(update.getCallbackQuery() != null)
            supported = true;

        Message msg = update.getMessage();
        if(!supported && msg != null && msg.getLocation() == null && msg.getText() != null && !msg.getText().startsWith("/")){
            supported = true;
        }
        return supported;
    }

    @Override
    public void handle(Update update) {
        CallbackQuery callback = update.getCallbackQuery();
        if(callback != null){
            Long chatId = callback.getMessage().getChatId();
            logger.info("Handling callback message \"{}\" from {}", callback.getData(), chatId);
            getAndReturnDepartures(chatId, new Stop(callback.getData(),""));
            return;
        }
        final String address = update.getMessage().getText();
        Long chatId = update.getMessage().getChatId();

        logger.info("Handling text message \"{}\" from {}", address, chatId);

        List<Stop> stops = getStopsForAddress(address, chatId);
        if (stops == null) return;
        switch (stops.size()){
            case 1:
                getAndReturnDepartures(update.getMessage().getChatId(), stops.get(0));
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                Utils.replyStopOptions(this.getBot(), update, stops);
                break;
            default:
                this.reply(chatId, "Adresse zu ungenau. Bitte gib die Adresse genauer ein!");
        }
    }

    @Nullable
    private List<Stop> getStopsForAddress(String address, Long chatId) {
        List<Stop> stops = null;
        try {
            stops = this.routingAPI.getStopsByAddress(address);
            if(stops == null || stops.isEmpty()){
                this.reply(chatId, "Es konnte keine Station zu der Adresse gefunden werden");
            }
        } catch (ApiException e) {
            this.reply(chatId, "Fehler bei der Stationssuche. Bitte versuche es später nochmal.");
        }
        return stops;
    }

    private void getAndReturnDepartures(Long chatId, Stop stop) {
        DepartureReturn departureReturn = null;
        try {
            departureReturn = this.monitorAPI.getDepartures(stop);
            String departure = departureReturn.toFormattedString();
            if(!StringUtils.isEmpty(stop.getId())){
                Optional<Stop> stopOptional = this.locationAPI.findStopById(stop.getId());
                if(stopOptional.isPresent())
                    departure = "*Abfahrten von: " + stopOptional.get().getName() + "*\n" + departure;
            }
            if(departure == null || StringUtils.equals(departure, ""))
                departure = "Es konnten keine Abfahrten gefunden werden!";
            reply(chatId, departure);
        } catch (ApiException e) {
            this.reply(chatId, "Fehler bei der Abfahrtssuche. Bitte versuche es später nochmal.");
        }
    }

    @Override
    public String getHelp() {
        return "Adresseingabe - Suche nach eingegebener Adresse";
    }
}
