package at.technikum.wien.syi.bot.handler;

import at.technikum.wien.syi.api.location.Address;
import at.technikum.wien.syi.api.location.LocationAPI;
import at.technikum.wien.syi.api.location.LocationWrapper;
import at.technikum.wien.syi.api.routing.Stop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

import static at.technikum.wien.syi.bot.utils.Utils.replyStopOptions;

public class LocationHandler extends Handler{
    static final Logger logger = LoggerFactory.getLogger(LocationHandler.class);
    private final LocationAPI locationAPI;

    public LocationHandler(TelegramLongPollingBot bot) {
        super(bot);
        this.locationAPI = new LocationWrapper();
    }

    @Override
    public boolean isSupported(Update update) {
        Message message = update.getMessage();
        return message != null && message.getLocation() != null;
    }

    @Override
    public void handle(Update update) {
        Location location = update.getMessage().getLocation();
        Long chatId = update.getMessage().getChatId();

        logger.info("Handling location message {} from {}", location.toString(), chatId);
        Address address = this.locationAPI.getAddressForCoordinates(location.getLatitude(), location.getLongitude());
        reply(chatId, "Aktueller Standort: " + address.getFullStreet());

        fetchAndReturnStopOptions(update, location, chatId);
    }

    private void fetchAndReturnStopOptions(Update update, Location location, Long chatId) {
        List<Stop> stops = this.locationAPI.getStopsForCoordinates(location.getLatitude(), location.getLongitude());
        if(stops == null || stops.isEmpty())
            reply(chatId, "Es wurden keine Haltestellen zur Adresse gefunden!");
        else
            replyStopOptions(this.getBot(), update, stops);
    }

    @Override
    public String getHelp() {
        return "Standort - Teile deinen Standort um nahe Abfahrten anzuzeigen";
    }
}
