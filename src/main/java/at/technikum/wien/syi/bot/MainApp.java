package at.technikum.wien.syi.bot;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MainApp {
    final static Logger logger = LoggerFactory.getLogger(MainApp.class);

    public static void main(String[] args) {
        logger.info("Telegram Monitor Bot Started");
        Properties prop = loadProperties();
        String username = prop.getProperty("telegram.username");
        String token = prop.getProperty("telegram.token");

        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try
        {
            telegramBotsApi.registerBot(new MonitorBot(username, token));
        } catch (TelegramApiRequestException e) {
            logger.error("Error when registering bot {0}", e);
        }

    }

    private static Properties loadProperties() {
        Properties prop = new Properties();

        String fileName = "app.config";
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(fileName);
        try {
            prop.load(is);
        } catch (IOException ex) {
            logger.error("Cannot read properties " + ex);
        }
        return prop;
    }

}
