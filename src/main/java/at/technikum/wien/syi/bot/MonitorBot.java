package at.technikum.wien.syi.bot;

import at.technikum.wien.syi.bot.handler.Handler;
import at.technikum.wien.syi.bot.handler.HelpHandler;
import at.technikum.wien.syi.bot.handler.LocationHandler;
import at.technikum.wien.syi.bot.handler.TextHandler;
import at.technikum.wien.syi.bot.utils.Utils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

public class MonitorBot extends TelegramLongPollingBot {
  static final Logger logger = LoggerFactory.getLogger(MonitorBot.class);

  private final String username;
  private final String token;

  private List<Handler> messageHandlerList;

  public MonitorBot(String username, String token) {
    this.username = username;
    this.token = token;
    registerHandlers();
  }

  private void registerHandlers() {
    messageHandlerList = Lists.newArrayList(
            new HelpHandler(this),
            new TextHandler(this),
            new LocationHandler(this));
  }

  @Override
  public void onUpdateReceived(Update update) {
    if(update.getMessage() != null)
      logger.info("Received message: {}", Utils.parseUpdateMessage(update.getMessage()));
    if(update.getCallbackQuery() != null)
      logger.info("Callback message: {}", Utils.parseCallbackMessage(update.getCallbackQuery()));

    boolean handled = false;
    for (Handler h : messageHandlerList) {
      if (h.isSupported(update)) {
        h.handle(update);
        handled = true;
        break;
      }
    }
    if (!handled) {
      logger.warn("Message could not be handled");
      Utils.replyMessage(this, update.getMessage().getChatId(), "Nachricht nicht unterstuetzt. /help um die Hilfe anzuzeigen.");
      Utils.replyAnimation(this,update, Utils.GIF_NOT_SUPPORTED);
    }
  }

  @Override
  public String getBotUsername() {
    return this.username;
  }

  @Override
  public String getBotToken() {
    return this.token;
  }
}
