package at.technikum.wien.syi.bot.utils;

import at.technikum.wien.syi.api.routing.Stop;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendAnimation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {
    final static Logger logger = LoggerFactory.getLogger(Utils.class);
    public static final String GIF_NOT_SUPPORTED = "https://media.giphy.com/media/lp8KjaSVDZEXccGidL/giphy.gif";

    public static void replyMessage(TelegramLongPollingBot bot, Long chatId, String message){
        logger.info("Replying {} to chat {}", message, chatId);
        try {
            message = message.replace("-","\\-")
                    .replace("!","\\!")
                    .replace(".","\\.")
                    .replace(">","\\>");
            bot.execute(new SendMessage(chatId, message).enableMarkdownV2(true));
        } catch (TelegramApiException e) {
            logger.error("Error when sending reply to {}", chatId);
        }
    }

    public static void replyAnimation(TelegramLongPollingBot bot, Update update, String animation){
        Long chatId = update.getMessage().getChatId();
        logger.info("Replying animation {} to chat {}", animation, chatId);
        SendAnimation sendAnimation = new SendAnimation();
        sendAnimation.setChatId(chatId);
        sendAnimation.setAnimation(animation);
        try {
            bot.execute(sendAnimation);
        } catch (TelegramApiException e) {
            logger.error("Error when sending reply to {}", chatId);
        }
    }

    public static void replyStopOptions(TelegramLongPollingBot bot, Update update, List<Stop> stops){
        Long chatId = update.getMessage().getChatId();
        logger.info("Replying options {} to chat {}", stops, chatId);

        List<List<InlineKeyboardButton>> lines = nullableList(stops).stream()
                .map(stop -> {
                    InlineKeyboardButton ikb = new InlineKeyboardButton(stop.getName());
                    ikb.setCallbackData(stop.getId());
                    return Lists.newArrayList(ikb);
                })
                .collect(Collectors.toList());
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup(lines);

        SendMessage sm = new SendMessage(chatId, "Meintest du eine der folgenden Haltestellen?");
        sm.setReplyMarkup(markup);
        try {
            bot.execute(sm);
        } catch (TelegramApiException e) {
            logger.error("Error when sending reply to {}", chatId);
        }
    }

    public static String parseUpdateMessage(Message message){
        StringBuilder sb = new StringBuilder();
        sb.append("ChatID: ");
        sb.append(message.getChatId());
        sb.append(" from ");
        sb.append(message.getFrom().getId());
        sb.append(" " + message.getFrom().getFirstName() );
        sb.append(" " + message.getFrom().getLastName() );
        sb.append(" : " + message.getText());

        return sb.toString();
    }

    public static String parseCallbackMessage(CallbackQuery message){
        StringBuilder sb = new StringBuilder();
        sb.append("ChatID: ");
        sb.append(message.getMessage().getChatId());
        sb.append(" from ");
        sb.append(message.getFrom().getId());
        sb.append(" " + message.getFrom().getFirstName() );
        sb.append(" " + message.getFrom().getLastName() );
        sb.append(" selected: " + message.getData() );

        return sb.toString();
    }

    public static <T> Collection<T> nullableList(Collection< T> coll) {
        return coll == null ? Lists.newArrayList() : coll;
    }
}
