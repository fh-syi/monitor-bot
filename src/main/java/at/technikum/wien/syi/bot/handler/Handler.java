package at.technikum.wien.syi.bot.handler;

import at.technikum.wien.syi.bot.utils.Utils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

public abstract class Handler {
    private TelegramLongPollingBot bot;

    public Handler(TelegramLongPollingBot bot) {
        this.bot = bot;
    }

    abstract public boolean isSupported(Update update);

    abstract public void handle(Update update);
    abstract String getHelp();

    void reply(Long chatId, String response){
        Utils.replyMessage(this.bot, chatId, response);
    }

    public TelegramLongPollingBot getBot() {
        return bot;
    }
}
